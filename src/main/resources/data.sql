DROP TABLE IF EXISTS users;
 
CREATE TABLE users (
  user_id long AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  username VARCHAR(250) NOT NULL,
  password VARCHAR(1000) NOT NULL,
  status boolean	
);

DROP TABLE IF EXISTS profile_information;

CREATE TABLE profile_information (
  user_id long PRIMARY KEY,
  address VARCHAR(250) NOT NULL,
  phone_number long
);

INSERT INTO users (first_name, last_name,username,password,status) VALUES
  ('Hari', 'Pillai','hari','{bcrypt}$2y$12$pPp7Nqmg2NMXFBMzwT3bGuVVmFlFe9nBiwbanjRYey0dJjBrqKhgO',True);
 


 
