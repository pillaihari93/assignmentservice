package com.amdocs.media.assignement.constants;

public class ErrorMsgConstant {

	// Authentication errors
		public static final String USERNAME_NOT_FOUND = "Requested username not found";
		public static final String INVALID_CREDENTIALS = "Invalid credentials";
		public static final String INVALID_JWT = "Invalid JWT";
		public static final String USER_CREDENTIALS_NOT_VALID = "Requested user is not valid";
		public static final String NO_LOGGED_IN_USER = "No logged in user";

}
