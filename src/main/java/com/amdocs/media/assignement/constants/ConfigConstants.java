package com.amdocs.media.assignement.constants;

public class ConfigConstants {

	public static final String HEADER_KEY_CONTENT_TYPE = "Content-Type";
	public static final String HEADER_VALUE_APPLICATION_JSON = "application/json";
	public static final String HEADER_KEY_AUTHORIZATION = "Authorization";
	public static final String ACCESS_TOKEN_PREFIX = "Bearer ";

}
