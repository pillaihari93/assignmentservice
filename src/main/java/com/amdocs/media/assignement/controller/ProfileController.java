package com.amdocs.media.assignement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.amdocs.media.assignement.dao.AssignmentResponse;
import com.amdocs.media.assignement.dao.Profile;
import com.amdocs.media.assignement.dao.ProfileRequest;
import com.amdocs.media.assignement.dao.User;
import com.amdocs.media.assignement.service.ProfileService;
import com.amdocs.media.assignement.service.UserService;
import com.amdocs.media.assignement.util.JwtUtils;
@RestController
class ProfileController {

@Autowired
UserService userService;
	
@Autowired
ProfileService profileService;

@Autowired 
JwtUtils jwtUtils;

@PostMapping("/profile")	
public AssignmentResponse insertProfile(@RequestHeader(value="Authorization")String header,@RequestBody ProfileRequest profileRequest) {
	AssignmentResponse response=new AssignmentResponse();
	
	try {
		String username=jwtUtils.transformHeader(header);
		User userdetail=(User)userService.loadUserByUsername(username);
		Profile profile=profileService.insertProfile(profileRequest.getAddress(), profileRequest.getPhoneNumber(), 
				userdetail.getUserId());
		response.setResponse(profile);
		response.setStatusCode(HttpStatus.OK.value());
		response.setMessage("Profile Inserted sucessfully");
		return response;
	}catch(Exception E) {
		response.setStatusCode(HttpStatus.BAD_REQUEST.value());
		response.setMessage(E.getMessage());
		return response;		
	}

	
}	
@DeleteMapping("/profile")
public AssignmentResponse deleteUserProfile(@RequestHeader(value="Authorization")String header) {
	AssignmentResponse response=new AssignmentResponse();
	
	try{
		String username=jwtUtils.transformHeader(header);
		User userdetail=(User)userService.loadUserByUsername(username);
		profileService.deleteProfile(userdetail.getUserId());
		response.setResponse(userdetail.getUsername());
		response.setStatusCode(HttpStatus.OK.value());
		response.setMessage("Profile Deleted sucessfully");
		return response;
	}
	catch(Exception E) {
		response.setStatusCode(HttpStatus.BAD_REQUEST.value());
		response.setMessage("No profile for user found");
		return response;		

	}
}

@PutMapping("/profile")	
public AssignmentResponse updateProfile(@RequestHeader(value="Authorization")String header,@RequestBody ProfileRequest profileRequest) {
	AssignmentResponse response=new AssignmentResponse();
	
	try {
		String username=jwtUtils.transformHeader(header);
		User userdetail=(User)userService.loadUserByUsername(username);
		Profile profile=profileService.insertProfile(profileRequest.getAddress(), profileRequest.getPhoneNumber(), 
				userdetail.getUserId());
		response.setResponse(profile);
		response.setStatusCode(HttpStatus.OK.value());
		response.setMessage("Profile Updated sucessfully");
		return response;
	}catch(Exception E) {
		response.setStatusCode(HttpStatus.BAD_REQUEST.value());
		response.setMessage(E.getMessage());
		return response;		
	}

	
}

}
