package com.amdocs.media.assignement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.amdocs.media.assignement.dao.AssignmentResponse;
import com.amdocs.media.assignement.dao.AuthenticationRequest;
import com.amdocs.media.assignement.dao.AuthenticationResponse;
import com.amdocs.media.assignement.dao.User;
import com.amdocs.media.assignement.dao.UserInfo;
import com.amdocs.media.assignement.service.UserService;
import com.amdocs.media.assignement.util.JwtUtils;

import javax.validation.constraints.NotNull;
import javax.xml.ws.http.HTTPBinding;


@RestController
public class AuthenticationController {

@Autowired
AuthenticationManager authenticationManager;

@Autowired
UserService userService;

@Autowired 
JwtUtils jwtUtils;

@PostMapping("/login")
public AssignmentResponse authenticateUser(@RequestBody @NotNull AuthenticationRequest request) throws Exception {
	
	String username = request.getUsername();
	String password = request.getPassword();
	
	try {
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
	} catch (Exception e) {
		System.out.println(e.getMessage());
		throw new Exception("USER not valid");
	}
	User authenticatedUser = (User) userService.loadUserByUsername(username);
	String jwt = jwtUtils.generateJWT(authenticatedUser, null);
	
	UserInfo userInfo=new UserInfo();
	userInfo.setFirstName(authenticatedUser.getFirstName());
	userInfo.setLastName(authenticatedUser.getLastName());
	
	AuthenticationResponse response=new AuthenticationResponse();
	response.setUserInfo(userInfo);
	response.setAccessToken(jwt);
	
	AssignmentResponse assignment=new AssignmentResponse();
	assignment.setResponse(response);
	assignment.setStatusCode(HttpStatus.OK.value());
	assignment.setMessage(HttpStatus.OK.name());
	
	return assignment;
}
	
}
