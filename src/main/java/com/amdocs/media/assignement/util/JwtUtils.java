package com.amdocs.media.assignement.util;

import java.util.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amdocs.media.assignement.constants.ConfigConstants;
import com.amdocs.media.assignement.constants.ErrorMsgConstant;
import com.amdocs.media.assignement.constants.JwtConstants;
import com.amdocs.media.assignement.dao.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * 
 * This class holds all functions related to JWT generation and validation. Also
 * it holds functions for extracting information from JWT
 * 
 * @author Hari
 *
 */
@Service
public class JwtUtils {

	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.token-validity-time}")
	private Long validityTime;

	/**
	 * This method generates jwt by accepting details of current logged-in user. It
	 * accepts additional claims other than default ones. It does following tasks 1.
	 * Calls a private method to generate claims. 2. Generates and returns JWT by
	 * setting claims.
	 * 
	 * @param userDetails
	 * @param additionalClaims
	 * @return
	 */
	public String generateJWT(User userDetails, Map<String, Object> additionalClaims) {

		Map<String, Object> claims = buildJwtClaims(userDetails, additionalClaims);
		String base64EncodedSecret = Base64.getEncoder().encodeToString(secret.getBytes());

		return Jwts.builder().setClaims(claims).setSubject(userDetails.getUsername())
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + validityTime))
				.signWith(SignatureAlgorithm.HS256, base64EncodedSecret).compact();
	}

	/**
	 * This method generates Map of claims for JWT. The default claims includes
	 * following parameters 1. user_id  2. org_id 3. email 4. additional
	 * claims passed in function parameter, if any
	 * 
	 * @param userDetails
	 * @param additionalClaims
	 * @return
	 */
	private Map<String, Object> buildJwtClaims(User userDetails, Map<String, Object> additionalClaims) {
		Map<String, Object> userDetailsClaims = new LinkedHashMap<>();
		userDetailsClaims.put(JwtConstants.KEY_USER_ID, userDetails.getUserId());

	
		userDetailsClaims.put(JwtConstants.KEY_NAME, userDetails.getFirstName());

		if (additionalClaims != null) {
			userDetailsClaims.putAll(additionalClaims);
		}

		return userDetailsClaims;
	}

	/**
	 * This method validates jwt and returns Claims encoded in JWT. It throws error
	 * if anything wrong with JWT
	 * 
	 * @param jwt
	 * @return
	 */
	public Claims validate(String jwt) {

		String base64EncodedSecret = Base64.getEncoder().encodeToString(secret.getBytes());

		return Jwts.parser().setSigningKey(base64EncodedSecret).parseClaimsJws(jwt).getBody();
	}

	/**
	 * This method returns username encoded in JWT. It calls validate() to extract
	 * claims and then return subject which is username
	 * 
	 * @param jwt
	 * @return
	 */
	public String extractUsername(String jwt) {
		Claims claims = validate(jwt);
		return claims.getSubject();
	}

	public String transformHeader(String accessToken) throws Exception {
		String username=null;
		if (accessToken != null && accessToken.startsWith(ConfigConstants.ACCESS_TOKEN_PREFIX)) {
			String jwt = accessToken.substring(ConfigConstants.ACCESS_TOKEN_PREFIX.length());
			if (jwt.trim().isEmpty()) {
				throw new Exception(ErrorMsgConstant.INVALID_JWT);
			}
			username = extractUsername(jwt);
		}
		return username;
	}
	
}
