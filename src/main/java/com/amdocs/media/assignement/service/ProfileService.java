package com.amdocs.media.assignement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amdocs.media.assignement.dao.Profile;
import com.amdocs.media.assignement.repository.ProfileRepository;

@Service
public class ProfileService {

	@Autowired
	ProfileRepository profileRepository;
	
	public Profile insertProfile(String address,long phoneNumber,long userId) throws Exception{
		
		Profile profile=new Profile();
		profile.setAddress(address);
		profile.setPhoneNumber(phoneNumber);
		profile.setUserId(userId);
		profileRepository.save(profile);
		return profile;
	}	
	
	
	public void deleteProfile(long userId) throws Exception {
		profileRepository.deleteById(userId);
		
	}
}
