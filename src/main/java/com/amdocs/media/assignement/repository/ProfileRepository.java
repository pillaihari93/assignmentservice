package com.amdocs.media.assignement.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.amdocs.media.assignement.dao.Profile;

@Repository
public interface ProfileRepository extends CrudRepository<Profile,Long>{

}
