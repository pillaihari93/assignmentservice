package com.amdocs.media.assignement.config;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport{
	 	@Bean
	    public Docket profileApi() {
	        return new Docket(DocumentationType.SWAGGER_2)
	        		.groupName("profile")
	                .select()
	                .apis(RequestHandlerSelectors.basePackage("com.amdocs.media.assignement.controller"))
	                .paths(regex("/profile.*"))
	                .build()
	                .apiInfo(metaData());
	    }
	 	@Bean
	    public Docket loginApi() {
	        return new Docket(DocumentationType.SWAGGER_2)
	        		.groupName("login")
	        		.select()
	                .apis(RequestHandlerSelectors.basePackage("com.amdocs.media.assignement.controller"))
	                .paths(regex("/login.*"))
	                .build()
	                .apiInfo(metaData());
	    }
	    private ApiInfo metaData() {
	        return new ApiInfoBuilder()
	                .title("Assignment")
	                .description("\"Amdoc assignment ")
	                .build();
	
	    }
	    
	    @Override
	    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
	        registry.addResourceHandler("swagger-ui.html")
	                .addResourceLocations("classpath:/META-INF/resources/");
	        registry.addResourceHandler("/webjars/**")
	                .addResourceLocations("classpath:/META-INF/resources/webjars/");
	    }

}
