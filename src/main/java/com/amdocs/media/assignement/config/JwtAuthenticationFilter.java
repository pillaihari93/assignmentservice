package com.amdocs.media.assignement.config;
import com.amdocs.media.assignement.constants.ConfigConstants;
import com.amdocs.media.assignement.constants.ErrorMsgConstant;
import com.amdocs.media.assignement.dao.AssignmentResponse;
import com.amdocs.media.assignement.dao.User;
import com.amdocs.media.assignement.service.UserService;
import com.amdocs.media.assignement.util.JwtUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	@Autowired
	private UserService userService;

	@Autowired
	private JwtUtils jwtUtils;

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			FilterChain filterChain) throws ServletException, IOException {

		final String accessToken = httpServletRequest.getHeader(ConfigConstants.HEADER_KEY_AUTHORIZATION);
		String username = null;
		String jwt;

		try {
			if (accessToken != null && accessToken.startsWith(ConfigConstants.ACCESS_TOKEN_PREFIX)) {
				jwt = accessToken.substring(ConfigConstants.ACCESS_TOKEN_PREFIX.length());
				if (jwt.trim().isEmpty()) {
					throw new Exception(ErrorMsgConstant.INVALID_JWT);
				}
				username = jwtUtils.extractUsername(jwt);
			}
			else if (accessToken !=null && !accessToken.startsWith(ConfigConstants.ACCESS_TOKEN_PREFIX)) {
				throw new Exception(ErrorMsgConstant.INVALID_JWT);
			}
			else if (accessToken == null) {
				filterChain.doFilter(httpServletRequest, httpServletResponse);
			}

		} catch (Exception e) {
			// TODO: proper exception handling. have to add logger
			// e.printStackTrace();
			setErrorResponse(httpServletResponse, ErrorMsgConstant.INVALID_JWT);
		}

		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			User userDetails = (User) userService.loadUserByUsername(username);

			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
					userDetails, null, userDetails.getAuthorities());
			usernamePasswordAuthenticationToken
					.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
			SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			filterChain.doFilter(httpServletRequest, httpServletResponse);
		}
	}

	private void setErrorResponse(HttpServletResponse httpServletResponse, String message) throws IOException {
		
		AssignmentResponse response = new AssignmentResponse();
		
		response.setMessage("Forbidden");
		response.setStatusCode(HttpStatus.FORBIDDEN.value());
		
		ObjectMapper objectMapper = new ObjectMapper();
		String assignmentResponseJsonString = objectMapper.writeValueAsString(response);

		httpServletResponse.setHeader(ConfigConstants.HEADER_KEY_CONTENT_TYPE,
				ConfigConstants.HEADER_VALUE_APPLICATION_JSON);
		httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
		httpServletResponse.getWriter().write(assignmentResponseJsonString);
	}

}
