package com.amdocs.media.assignement.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.amdocs.media.assignement.constants.ConfigConstants;
import com.amdocs.media.assignement.dao.AssignmentResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			AuthenticationException e) throws IOException, ServletException {

		System.out.println(httpServletResponse.getStatus());

		if (httpServletResponse.getHeader(ConfigConstants.HEADER_KEY_CONTENT_TYPE) == null) {

			System.out.println("value null");
			AssignmentResponse response = new AssignmentResponse();
			
			response.setMessage("Forbidden");
			response.setStatusCode(HttpStatus.FORBIDDEN.value());
			
			ObjectMapper objectMapper = new ObjectMapper();
			String assignmentResponseJsonString = objectMapper.writeValueAsString(response);

			httpServletResponse.setHeader(ConfigConstants.HEADER_KEY_CONTENT_TYPE,
					ConfigConstants.HEADER_VALUE_APPLICATION_JSON);
			httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
			httpServletResponse.getWriter().write(assignmentResponseJsonString);

		}

	}
}
